FROM python:3
ENV http_proxy http://10.195.54.53:80
ENV https_proxy http://10.195.54.53:80
ADD . /
RUN pip install flask
RUN pip install flask_restful
EXPOSE 5000
CMD [ "python", "./patch.py"]
